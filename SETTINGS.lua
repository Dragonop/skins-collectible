-- BEWARE: if you've already launched your server featuring this mod, changing
-- these fields is COMPLETELY USELESS. If that's the case, you must edit them
-- directly through your minetest.conf (they all start with `skinsc_`)

-- skins unlocked by default
local default_skins = {1, 2, 3, 4}





----------------------------------------------
--------------------.CONF---------------------
----------------------------------------------

local settings = minetest.settings

if settings:get("skinsc_default_skins") == nil then
  settings:set("skinsc_default_skins", minetest.serialize(default_skins))
end
