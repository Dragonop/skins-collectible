local S = minetest.get_translator("skins_collectible")

minetest.register_tool("skins_collectible:wardrobe", {

  description = S("Wardrobe"),
  inventory_image = "skinscollectible_wardrobe.png",
  groups = {oddly_breakable_by_hand = "2"},
  on_place = function() end,
  on_drop = function() end,

  on_use = function(itemstack, user, pointed_thing)
    local p_name = user:get_player_name()
    minetest.show_formspec(p_name, "skins_collectible:GUI", skins_collectible.get_formspec(p_name, 1, skins_collectible.get_player_skin_ID(p_name)))
  end

})
