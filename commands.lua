ChatCmdBuilder.new("skinsc", function(cmd)

    -- sblocco skin
    cmd:sub("unlock :playername :skinID:int", function(sender, p_name, skinID)

      local had_skin = skins_collectible.is_skin_unlocked(p_name, skinID)
      local _, error = skins_collectible.unlock_skin(p_name, skinID)

      if error then
        minetest.chat_send_player(sender, error)
        return end

      if had_skin then
        minetest.chat_send_player(sender, "Player " .. p_name .. " already has this skin!")
        return end

      minetest.chat_send_player(sender, "Skin " .. skins_collectible.get_skin(skinID).name .. " for " .. p_name .. " successfully unlocked!")
    end)

    -- rimozione skin
    cmd:sub("remove :playername :skinID:int", function(sender, p_name, skinID)

      local had_skin = skins_collectible.is_skin_unlocked(p_name, skinID)
      local _, error = skins_collectible.remove_skin(p_name, skinID)

      if error then
        minetest.chat_send_player(sender, error)
        return end

      if not had_skin then
        minetest.chat_send_player(sender, "Player " .. p_name .. " doesn't have this skin!")
        return end

      minetest.chat_send_player(sender, "Skin " .. skins_collectible.get_skin(skinID).name .. " for " .. p_name .. " successfully removed")
    end)

end, {
  description = "Manage players skins",
  privs = { skinscollectible_admin = true }
})
