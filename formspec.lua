
local function FS() return end
local S = minetest.get_translator("skins_collectible")

local locked_skin = {
  name        = "???",
  description = "???",
  model       = nil,
  texture     = nil,
  tier        = "???",
  img         = "skinscollectible_gui_silhouette_unknown.png",
  author      = "",
  license     = ""
}


function skins_collectible.get_formspec(p_name, page, skin_ID)

  local selected_skin
  local preview
  local tier_bg = "blank.png"
  local tier_bg2 = "blank.png"

  -- se la skin è bloccata o meno
  if skin_ID == "LOCKED" then
    selected_skin = locked_skin
    preview = "skinscollectible_locked.png"
  else
    selected_skin = skins_collectible.get_skin(skin_ID)
    preview = skins_collectible.get_preview(skin_ID)
    if selected_skin.tier > 2 then
      tier_bg = "skinscollectible_gui_bg_tier3.png"
      --tier_bg2 = "skinscollectible_gui_wave.png"
    end

    minetest.get_player_by_name(p_name):get_meta():set_int("skins_collectible:selected_skin_ID", skin_ID)    -- metadato per "Wear" se è sbloccata
  end

  local formspec = {
    -- immagini
    "image[0,0;16.15,9.24;" .. tier_bg .. "]",
    "image[0,0;16.15,9.24;" .. tier_bg2 .. "]",
    "image[0,0;16.15,9.24;" .. selected_skin.img .. "]",
    "image[0,0;16.15,9.24;skinscollectible_gui_overlay.png]",
    -- skin selezionata
    "image[1.92,0.92;1.5,2.34;" .. preview .. "]",
    -- pulsanti
    "image_button[2,3.2;1.3,0.7;skinscollectible_gui_button_wear.png;WEAR;" .. S("Wear") .. "]",
    "image_button[0.2,6;0.7,1.2;skinscollectible_gui_arrow_left.png;GO_LEFT;]",
    "image_button[15.2,6;0.7,1.2;skinscollectible_gui_arrow_right.png;GO_RIGHT;]",
    -- testo
    "hypertext[3.6,0.92;4,1;name; <global size=24 font=mono color=#62b2cd><b>" .. selected_skin.name .. "</b>]",
    --"hypertext[3.6,1.3;3.2,2;desc; <global size=13 font=mono halign=justify color=#62b2cd><i>" .. FS(selected_skin.description) .. "</i>]", -- doesn't work
    "hypertext[3.6,1.3;3.2,2;desc; <global size=13 font=mono halign=justify color=#62b2cd><i>" .. selected_skin.description .. "</i>]",
    "hypertext[3.6,3.1;3,1;license; <global size=12 font=mono color=#71aa34>" .. selected_skin.license .. "]",
    "hypertext[3.6,3.3;3,1;author; <global size=12 font=mono color=#71aa34>" .. S("By:") .. selected_skin.author .. "]",
  }

  local skins_amount = skins_collectible.get_loaded_skins_amount()
  local first_idx = (math.floor(skins_amount / 16) + 1) * page

  -- creo slot in matrice x*y
  for y = 1, 2 do
    for x = 1, 8 do

      local skin_ID = (x + (8 * (y-1))) * first_idx

      -- se ho raggiunto il numero massimo di skin, interrompo
      if skin_ID > skins_amount then
        break
      end

      local size_x = 1.603
      local size_y = 2.462
      local pos_x = size_x * x
      local pos_y = size_y * y
      local indent_x = 0.04
      local indent_y = 1.8
      local slot_size = size_x .. "," .. size_y .. ";"
      local slot_pos = indent_x + pos_x ..  "," .. indent_y + pos_y .. ";"

      if skins_collectible.is_skin_unlocked(p_name, x) then
        table.insert(formspec, skin_ID, "image_button[" .. slot_pos .. slot_size .. skins_collectible.get_preview(skin_ID) .. ";" .. skin_ID .. ";]")
      else
        table.insert(formspec, skin_ID, "image_button[" .. slot_pos .. slot_size .. "skinscollectible_gui_button.png;LOCKED;]")
        table.insert(formspec, skin_ID +1, "tooltip[" .. slot_pos .. slot_size .. FS(skins_collectible.get_skin(skin_ID).hint) .. " ;#dff6f5;#5a5353]")
      end
    end
  end

  -- aggiunte finali che hanno la priorità
  table.insert(formspec, 1, "formspec_version[4]")
  table.insert(formspec, 2, "size[16.15,9.24]")
  table.insert(formspec, 3, "no_prepend[]")
  table.insert(formspec, 4, "background[0,0;16.15,9.24;skinscollectible_gui_bg.png]")
  table.insert(formspec, 5, "style_type[image_button;border=false]")
  table.insert(formspec, 6, "style[wear;font=mono;textcolor=#dff6f5]")
  table.insert(formspec, 7, "bgcolor[;true]")



  return table.concat(formspec, "")
end





function FS(txt)
	return minetest.formspec_escape(S(txt))
end
