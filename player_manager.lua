minetest.register_on_joinplayer(function(player)
  skins_collectible.load_player_data(player)
end)



minetest.register_on_player_receive_fields(function(player, formname, fields)

  if formname ~= "skins_collectible:GUI" then return end
  if fields.quit then return end

  local p_name = player:get_player_name()

  -- se provo a indossarla
  if fields.WEAR then
    local skin_ID = player:get_meta():get_int("skins_collectible:selected_skin_ID")

    -- se è sbloccata, indosso la skin e chiudo il formspec
    if skins_collectible.is_skin_unlocked(p_name, skin_ID) then
      skins_collectible.set_skin(player, skin_ID, true)
      minetest.close_formspec(p_name, "skins_collectible:GUI")
      --TODO: suono + effetto particellare
    -- sennò riproduco un suono d'errore
    else
      -- TODO: suono negativo
    end

  -- se provo a cambiar pagina
  elseif fields.GO_LEFT then
  elseif fields.GO_RIGHT then

  -- selezione skin
  elseif fields.LOCKED then
    minetest.show_formspec(p_name, "skins_collectible:GUI", skins_collectible.get_formspec(p_name, 1, "LOCKED"))
  else
    local skin_ID = tonumber(string.match(dump(fields), "(%d+)"))
    minetest.show_formspec(p_name, "skins_collectible:GUI", skins_collectible.get_formspec(p_name, 1, skin_ID))
  end

end)
